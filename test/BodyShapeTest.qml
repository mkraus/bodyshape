import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQml.Models 2.1

import "../qml/pages"
import "../qml/pages/Database.js" as Database

ApplicationWindow {
    id: root
    width: 540
    height: 960
    color: "white"
    
    property date currentDate: new Date()

    property var dataSets: []
    property var unitsWeightChart: ({x: "", y: "kg"})
    property var unitsFatChart: ({x: "", y: "kg"})
    property var unitsMuscleChart: ({x: "", y: "kg"})
    property var labelsX: ({min: "0", max: "10"})

    property bool inhibitRepaint: true
    
    property int chartHeight: (column.height - secondToolBar.height - bodyShapeListHeader.height)/4 - column.spacing
    property bool showDots: showDots.checked
    property string lineType: "bezier"

    function repaintCharts() {
        if(root.inhibitRepaint === false) {
            // get data sets
            root.dataSets = Database.getDataSets(minWeight.value, maxWeight.value);

            // get units and labels if there are datasets
            if (root.dataSets.length > 0) {
                // get units
                var units = Database.getUnits();
                root.unitsWeightChart = {x: "", y: units.weight};
                root.unitsFatChart = {x: "", y: units.fat};
                root.unitsMuscleChart = {x: "", y: units.muscle};

                // get x labels
                var minDate = Database.getDateFromDayId(dataSets[0].dayId);
                root.labelsX.min = (minDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat));
                var maxDate = Database.getDateFromDayId(dataSets[dataSets.length-1].dayId);
                root.labelsX.max = (maxDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat));
            }

            // update charts
            weightChart.updateChart();
            fatChart.updateChart();
            muscleChart.updateChart();
        }
    }

    function addRandomDataSet(date) {
        var dayId = Database.getDayIdFromDate(date);
        Database.addDataSet(dayId, 802, (70 + Math.random() * 10).toFixed(2), (12 + Math.random()).toFixed(2), (45 + Math.random()).toFixed(2), "chosen by fair dice roll");
    }

    Component.onCompleted: {
        //Database.deleteAllDataSets();
        //importFromFile();
        //addRandomData(60);
        root.dataSets = Database.getAllDataSets();

        Database.setUpdateCallback(repaintCharts);

        root.inhibitRepaint = false;
        repaintCharts();
    }

    toolBar: ToolBar {
        id: mainToolBar
        width: parent.width
        RowLayout {
            id: rowLayout
            width: parent.width

            CheckBox {
                id: showDots
                checked: true
                text: "Dots"

                onClicked: {
                    repaintCharts();
                }
            }

            SpinBox {
                id: bezierFactor
                decimals: 2
                minimumValue: 0
                maximumValue: 1
                stepSize: 0.01
                value: 0.25

                onValueChanged: {
                    repaintCharts();
                }
            }

            SpinBox {
                id: slopeFactor
                decimals: 2
                minimumValue: 1
                maximumValue: 2
                stepSize: 0.01
                value: 1.25

                onValueChanged: {
                    repaintCharts();
                }
            }

            ComboBox {
                id: presentation
                currentIndex: Database.getSetting("fatPresentation", "relative") === "relative" ? 0 : 1;
                model: ListModel {
                    id: presentationItems
                    ListElement { text: "%"; type: "relative" }
                    ListElement { text: "kg"; type: "absolute" }
                }
                width: 200
                onCurrentIndexChanged: {
                    Database.setSetting("fatPresentation", presentationItems.get(currentIndex).type);
                    Database.setSetting("musclePresentation", presentationItems.get(currentIndex).type);
                    Database.getAllDataSets();
                    repaintCharts();
                }
            }

            CheckBox {
                id: sma
                checked: Number(Database.getSetting("SmaPoints", "1")) > 1
                text: "SMA"

                onClicked: {
                    Database.setSetting("SmaPoints", sma.checked ? "10" : "1");
                    repaintCharts();
                }
            }

            Item {
                id: spacer
                Layout.fillWidth: true
            }

            ComboBox {
                id: cbox
                currentIndex: root.lineType === "line" ? 0 : 1;
                model: ListModel {
                    id: cbItems
                    ListElement { text: "Line"; type: "line" }
                    ListElement { text: "Bezier"; type: "bezier" }
                }
                width: 200
                onCurrentIndexChanged: {
                    root.lineType = cbItems.get(currentIndex).type;
                    repaintCharts();
                }
            }
        }
    }
    
    Column {
        id: column
        anchors.fill: parent
        spacing: 10

        Row {
            id: secondToolBar
            width: parent.width
            //height: 20

            Button {
                id: addRandomDataSetButton
                text: "Add Random"

                onClicked: {
                    addRandomDataSet(new Date());
                }
            }


            Button {
                id: addDataSetButton
                text: "Add"

                onClicked: {
                    var date = new Date();
                    Database.addDataSet(Database.getDayIdFromDate(date), Database.getTimeIdFromDate(date), weightDataToAdd.value, fatDataToAdd.value, muscleDataToAdd.value, "");
                }
            }
            SpinBox {
                id: weightDataToAdd
                decimals: 2
                minimumValue: 10
                maximumValue: 200
                stepSize: 0.1
                value: 73
            }
            SpinBox {
                id: fatDataToAdd
                decimals: 2
                minimumValue: 1
                maximumValue: 99
                stepSize: 0.1
                value: 12
            }
            SpinBox {
                id: muscleDataToAdd
                decimals: 2
                minimumValue: 1
                maximumValue: 99
                stepSize: 0.1
                value: 46
            }

            Button {
                id: filterDataSets
                text: "Filter"

                onClicked: repaintCharts();
            }
            SpinBox {
                id: minWeight
                decimals: 2
                minimumValue: 0
                maximumValue: 1000
                stepSize: 0.1
                value: 0
            }
            SpinBox {
                id: maxWeight
                decimals: 2
                minimumValue: 0
                maximumValue: 1000
                stepSize: 0.1
                value: 1000
            }
        }
        
        Chart {
            id: weightChart
            width: parent.width
            height: chartHeight

            chartTitle: "Weight"
            chartEmptyInfoText: "no datasets available"
            dataSets: root.dataSets
            units: root.unitsWeightChart
            labelsX: root.labelsX
            showLabelX: false
            showLabelY: true
            dataIds: ({x: "dayId", y: "weight"})
            lineType: root.lineType
            showDots: root.showDots
            bezierFactor: bezierFactor.value
            slopeFactor: slopeFactor.value
        }
        Chart {
            id: fatChart
            width: parent.width
            height: chartHeight

            chartTitle: "Body Fat"
            dataSets: root.dataSets
            units: root.unitsFatChart
            labelsX: root.labelsX
            showLabelX: false
            showLabelY: true
            dataIds: ({x: "dayId", y: "fat"})
            lineType: root.lineType
            showDots: root.showDots
            bezierFactor: bezierFactor.value
            slopeFactor: slopeFactor.value
        }
        Chart {
            id: muscleChart
            width: parent.width
            height: chartHeight

            chartTitle: "Muscle Mass"
            dataSets: root.dataSets
            units: root.unitsMuscleChart
            labelsX: root.labelsX
            showLabelX: true
            showLabelY: true
            dataIds: ({x: "dayId", y: "muscle"})
            lineType: root.lineType
            showDots: root.showDots
            bezierFactor: bezierFactor.value
            slopeFactor: slopeFactor.value
        }

        Component.onCompleted: {
            weightChart.signalShowToolTip.connect(fatChart.showToolTip);
            weightChart.signalHideToolTip.connect(fatChart.hideToolTip);
            weightChart.signalShowToolTip.connect(muscleChart.showToolTip);
            weightChart.signalHideToolTip.connect(muscleChart.hideToolTip);

            fatChart.signalShowToolTip.connect(weightChart.showToolTip);
            fatChart.signalHideToolTip.connect(weightChart.hideToolTip);
            fatChart.signalShowToolTip.connect(muscleChart.showToolTip);
            fatChart.signalHideToolTip.connect(muscleChart.hideToolTip);

            muscleChart.signalShowToolTip.connect(weightChart.showToolTip);
            muscleChart.signalHideToolTip.connect(weightChart.hideToolTip);
            muscleChart.signalShowToolTip.connect(fatChart.showToolTip);
            muscleChart.signalHideToolTip.connect(fatChart.hideToolTip);
        }

        Row {
            id: bodyShapeListHeader
            width: parent.width
            Label {
                width: bodyShapeList.dateTimeWidth
                text: "Date"
            }
            Label {
                width: bodyShapeList.weightWidth
                text: "Weight [" + root.unitsWeightChart.y + "]"
            }
            Label {
                width: bodyShapeList.fatWidth
                text: "Fat [" + root.unitsFatChart.y + "]"
            }
            Label {
                width: bodyShapeList.muscleWidth
                text: "Muscle [" + root.unitsFatChart.y + "]"
            }
        }

        ListView {
            id: bodyShapeList
            width: parent.width
            height: chartHeight - bodyShapeListHeader.height
            verticalLayoutDirection: ListView.BottomToTop
            highlightFollowsCurrentItem: true
            model: root.dataSets
            clip: true

            property int dateTimeLength: 1
            property int weightLength: 1
            property int fatLength: 1
            property int muscleLength: 1

            property int dateTimeWidth: dateTimeLength / (dateTimeLength + weightLength + fatLength + muscleLength) * width
            property int weightWidth: weightLength / (dateTimeLength + weightLength + fatLength + muscleLength) * width
            property int fatWidth: fatLength / (dateTimeLength + weightLength + fatLength + muscleLength) * width
            property int muscleWidth: muscleLength / (dateTimeLength + weightLength + fatLength + muscleLength) * width

            delegate: Row {
                width: parent.width
                property date dateTime: Database.getDateFromDayAndTimeId(modelData.dayId, modelData.timeId)
                Label {
                    id: dateTimeLabel
                    width: bodyShapeList.dateTimeWidth
                    text: dateTime.toLocaleString(Qt.locale(), Locale.ShortFormat)
                }
                Label {
                    id: weightLabel
                    width: bodyShapeList.weightWidth
                    text: modelData.weight.toFixed(2).replace(".", Qt.locale().decimalPoint);
                }
                Label {
                    id: fatLabel
                    width: bodyShapeList.fatWidth
                    text: modelData.fat.toFixed(2).replace(".", Qt.locale().decimalPoint);
                }
                Label {
                    id: muscleLabel
                    width: bodyShapeList.muscleWidth
                    text: modelData.muscle.toFixed(2).replace(".", Qt.locale().decimalPoint);
                }

                Component.onCompleted: {
                    if(bodyShapeList.dateTimeLength < dateTimeLabel.text.length) bodyShapeList.dateTimeLength = dateTimeLabel.text.length
                    if(bodyShapeList.weightLength < weightLabel.text.length) bodyShapeList.weightLength = weightLabel.text.length
                    if(bodyShapeList.fatLength < fatLabel.text.length) bodyShapeList.fatLength = fatLabel.text.length
                    if(bodyShapeList.muscleLength < muscleLabel.text.length) bodyShapeList.muscleLength = muscleLabel.text.length
                }
            }
            onCountChanged: {positionViewAtEnd()}
        }
    }
    
    function importFromFile() {
        
        var days = new Date(2015, 05, 01) / (24*60*60*1000);
        
        var date;
        var dateString = "";
        var dateId = 0;
        var timeString = "";
        var timeId = 0;
        var weight = 0;
        var fat = 0;
        var muscle = 0;
        var index = 0;
        root.inhibitRepaint = true
        for (index = 0; index < ValueLogger.Gewicht.length; index++)
        {
            dateString = ValueLogger.Gewicht[index].slice(0, ValueLogger.Gewicht[index].indexOf(" "));
            timeString = ValueLogger.Gewicht[index].slice(ValueLogger.Gewicht[index].indexOf(" ")+1, ValueLogger.Gewicht[index].indexOf(";"));
            date = new Date(dateString.slice(0, 4), dateString.slice(5,7)-1 /*january is 0*/, dateString.slice(8, 10), timeString.slice(0, timeString.indexOf(":")), timeString.slice(timeString.indexOf(":")+1, timeString.lastIndexOf(":")));

            weight = ValueLogger.Gewicht[index].slice(ValueLogger.Gewicht[index].indexOf(";")+1).replace(",", ".");
            fat = ValueLogger.Koerperfett[index].slice(ValueLogger.Koerperfett[index].indexOf(";")+1).replace(",", ".");
            muscle = ValueLogger.Muskelmasse[index].slice(ValueLogger.Muskelmasse[index].indexOf(";")+1).replace(",", ".");
            Database.addDataSet(Database.getDayIdFromDate(date), Database.getTimeIdFromDate(date), weight, fat, muscle, "");
        }
        root.inhibitRepaint = false
    }

    function addRandomData(numberOfDays) {
        var dayIdStart = Database.getDayIdFromDate(new Date()) - numberOfDays;
        root.inhibitRepaint = true;
        for(var i = dayIdStart; i < (dayIdStart + numberOfDays); i++) {
            addRandomDataSet(Database.getDateFromDayId(i));
        }
        root.inhibitRepaint = false
    }

}
