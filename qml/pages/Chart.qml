/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0

import "Database.js" as Database

Canvas {
    id: canvas

    property var dataSets: []
    property var dataIds: ({x: "x", y: "y"})
    property var units: ({x: "d", y: "kg"})
    property var labelsX: ({min: "", max: ""})
    property bool showLabelX: false
    property bool showLabelY: false
    property bool showGrid: true

    property bool showDots: true
    property string lineType: "bezier"
    property real bezierFactor: 0.25
    property real slopeFactor: 1.25
    property int lineWidth: 3
    property int chartMargin: 4*lineWidth
    property string lineColor: "grey"
    property string dotColor: "white"
    property string gridColor: "lightgrey"
    property string labelColor: "grey"
    property string labelFontFamily: "sans serif"
    property int labelFontSizeTitle: 12
    property int labelFontSize: 10
    property string chartTitle: ""
    property string chartEmptyInfoText: ""

    signal signalShowToolTip(int dataIndex, int eventPositionX, int eventPositionY)
    signal signalHideToolTip()

    // TODO: make this properties private
    property real xStep: 1
    property real yStep: 1
    property real maxScale: 0

    function updateChart() {
        clearChart();
        hideToolTip(false);
        canvas.requestPaint();
    }

    function clearChart() {
        if (!canvas.available) return;

        var ctx = canvas.getContext('2d');
        if (ctx === null) return;

        ctx.save();
        ctx.clearRect(canvas.canvasWindow.x, canvas.canvasWindow.y, canvas.canvasWindow.width, canvas.canvasWindow.height);
        ctx.restore();
    }

    Component.onCompleted: {
        updateChart();
    }

    function findMinMax(dataSets, dataId) {
        var min = dataSets[0][dataId];
        var max = dataSets[0][dataId];
        for (var i = 1; i < dataSets.length; i++) {
            if(dataSets[i][dataId] < min) min = dataSets[i][dataId];
            if(dataSets[i][dataId] > max) max = dataSets[i][dataId];
        }

        return {min: min, max: max};
    }

    function drawGrid(ctx, minScale, maxScale) {

        var deltaY = (canvas.height - 2*chartMargin) / 4;

        ctx.save();
        ctx.globalAlpha = 0.25;
        ctx.strokeStyle = canvas.gridColor;
        ctx.lineWidth = canvas.lineWidth - 2;

        // horizontal grid lines
        for (var i = 0; i * deltaY <= canvas.height - 2*chartMargin; i++) {
            ctx.beginPath();
            ctx.moveTo(Math.round(chartMargin/5), Math.round(chartMargin + i * deltaY));
            ctx.lineTo(Math.round(canvas.width - chartMargin/5), Math.round(chartMargin + i * deltaY));
            ctx.stroke();
        }

        ctx.globalAlpha = 0.5;
        ctx.strokeStyle = canvas.scaleColor;
        ctx.lineWidth = canvas.lineWidth - 1;

        // y-scale
        ctx.beginPath();
        ctx.moveTo(Math.round(chartMargin/2), Math.round(chartMargin/5));
        ctx.lineTo(Math.round(chartMargin/2), Math.round(canvas.height - chartMargin/5));
        ctx.stroke();

        ctx.restore();
    }

    function drawLabels(ctx, minScale, maxScale) {
        ctx.save();
        ctx.globalAlpha = 0.8;

        if(canvas.chartTitle.length > 0) {
            ctx.fillStyle = canvas.labelColor;
            ctx.font = "normal " + canvas.labelFontSizeTitle + "px " + canvas.labelFontFamily;

            ctx.beginPath();
            ctx.textAlign = "center";
            ctx.fillText(canvas.chartTitle, canvas.width/2, chartMargin - canvas.lineWidth);
            ctx.stroke();
        }

        if(canvas.showLabelX === true) {
            ctx.fillStyle = canvas.labelColor;
            ctx.font = "normal " + canvas.labelFontSize + "px " + canvas.labelFontFamily;

            ctx.beginPath();
            ctx.textAlign = "left";
            ctx.fillText(canvas.labelsX.min, chartMargin, canvas.height);
            ctx.stroke();
            ctx.beginPath();
            ctx.textAlign = "right";
            ctx.fillText(canvas.labelsX.max, canvas.width - chartMargin, canvas.height);
            ctx.stroke();
        }

        if(canvas.showLabelY === true) {
            ctx.fillStyle = canvas.labelColor;
            ctx.textAlign = "left";
            ctx.font = "normal " + canvas.labelFontSize + "px " + canvas.labelFontFamily;

            ctx.beginPath();
            ctx.fillText(maxScale + canvas.units.y, chartMargin, chartMargin - canvas.lineWidth);
            ctx.fillText(minScale + canvas.units.y, chartMargin, canvas.height - chartMargin - canvas.lineWidth);
            ctx.stroke();
        }

        ctx.restore();
    }

    function drawDots(ctx, dataSets, maxScale, xStep, yStep) {
        var chartMargin = canvas.chartMargin
        var lineWidth = canvas.lineWidth;
        var dotRadius = lineWidth/4;

        var alpha = 1;

        // fade out points if they get too near to each other
        if(xStep < 3*lineWidth) {
            if(xStep < 1.5*lineWidth) {
                alpha = 0;
                return;
            }
            else {
                alpha = alpha * (xStep - 1.5*lineWidth)/(1.5*lineWidth);
            }
        }

        ctx.save();
        ctx.globalAlpha = alpha;
        ctx.strokeStyle = canvas.dotColor;
        ctx.fillStyle = canvas.dotColor;
        ctx.lineWidth = lineWidth;

        if(dataSets.length < 2) {
            ctx.beginPath();
            ctx.arc(chartMargin + xStep/2, chartMargin + (maxScale - dataSets[0][canvas.dataIds.y]) * yStep, dotRadius, 0, 2*Math.PI);
            ctx.fill();
            ctx.stroke();
        }
        else {
            for (var i = 0; i < dataSets.length; i++) {
                ctx.beginPath();
                ctx.arc(chartMargin + (dataSets[i][canvas.dataIds.x] - dataSets[0][canvas.dataIds.x]) * xStep, chartMargin + (maxScale - dataSets[i][canvas.dataIds.y]) * yStep, dotRadius, 0, 2*Math.PI);
                ctx.fill();
                ctx.stroke();
            }
        }
        ctx.restore();
    }

    function drawLineGraph(ctx, dataSets, maxScale, xStep, yStep) {
        var chartMargin = canvas.chartMargin

        ctx.save();
        ctx.globalAlpha = 0.8;
        ctx.strokeStyle = canvas.lineColor;
        ctx.lineWidth = canvas.lineWidth;
        ctx.lineJoin = "bevel";

        ctx.beginPath();
        ctx.moveTo(chartMargin, chartMargin + (maxScale - dataSets[0][canvas.dataIds.y]) * yStep);
        for(var i = 1; i < dataSets.length; i++) {
            ctx.lineTo(chartMargin + (dataSets[i][canvas.dataIds.x] - dataSets[0][canvas.dataIds.x]) * xStep, chartMargin + (maxScale - dataSets[i][canvas.dataIds.y]) * yStep);
        }
        ctx.stroke();
        ctx.restore();
    }

    function drawBezierGraph(ctx, dataSets, maxScale, xStep, yStep) {
        var chartMargin = canvas.chartMargin

        var bezierFactor = canvas.bezierFactor;
        var slopeFactor = canvas.slopeFactor;

        var i = 0;
        var previousX = 0;
        var currentX = chartMargin;
        var nextX = chartMargin + (dataSets[i+1][canvas.dataIds.x] - dataSets[0][canvas.dataIds.x]) * xStep;
        var previousY = 0;
        var currentY = chartMargin + (maxScale - dataSets[i][canvas.dataIds.y]) * yStep;
        var nextY = chartMargin + (maxScale - dataSets[i+1][canvas.dataIds.y]) * yStep;

        var deltaX = (nextX - currentX) * bezierFactor;
        var deltaY = 0;
        var slope = 0;
        var cp1 = {x: currentX + deltaX, y: currentY + deltaY};
        var cp2 = {x: 0, y: 0};

        ctx.save();
        ctx.globalAlpha = 0.8;
        ctx.strokeStyle = canvas.lineColor;
        ctx.lineWidth = canvas.lineWidth;

        ctx.beginPath();
        ctx.moveTo(currentX, currentY);
        for (i = 1; i < dataSets.length-1; i++) {
            previousX = currentX;
            currentX = nextX;
            nextX = chartMargin + (dataSets[i+1][canvas.dataIds.x] - dataSets[0][canvas.dataIds.x]) * xStep;
            previousY = currentY;
            currentY = nextY;
            nextY = chartMargin + (maxScale - dataSets[i+1][canvas.dataIds.y]) * yStep;

            slope = (nextY - previousY) / (nextX - previousX) * slopeFactor;
            deltaX = (currentX - previousX) * bezierFactor;
            deltaY = deltaX * slope;
            cp2 = {x: currentX - deltaX, y: currentY - deltaY};
            ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y , currentX, currentY);
            deltaX = (nextX - currentX) * bezierFactor;
            deltaY = deltaX * slope;
            cp1 = {x: currentX + deltaX, y: currentY + deltaY};
        }
        i = dataSets.length-1;
        previousX = currentX;
        currentX = nextX;
        previousY = currentY;
        currentY = nextY;

        deltaX = (currentX - previousX) * bezierFactor;
        deltaY = 0;
        cp2 = {x: currentX - deltaX, y: currentY - deltaY};
        ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y , currentX, currentY);
        ctx.stroke();
        ctx.restore();
    }

    onPaint:{
        if (!canvas.available) return;

        var ctx = canvas.getContext('2d');
        if (ctx === null) return;

        var dataSets = canvas.dataSets;
        if(dataSets.length === 0) return;

        if(dataSets.length < 2) {
            canvas.xStep = canvas.width - 2*canvas.chartMargin;
        }
        else {
            canvas.xStep = (canvas.width - 2*canvas.chartMargin) / (dataSets[dataSets.length-1][canvas.dataIds.x] - dataSets[0][canvas.dataIds.x]);
        }

        var minMax = findMinMax(dataSets, canvas.dataIds.y);
        var minScale = Math.floor(minMax.min * 10) / 10;
        var maxScale = Math.ceil(minMax.max * 10) / 10;
        if(minScale === maxScale) {
            minScale = Math.floor((minMax.min-0.1) * 10) / 10;
            maxScale = Math.ceil((minMax.max+0.1) * 10) / 10;
        }
        else if(minMax.max - minMax.min < 1) {
            minScale = Math.floor(minMax.min * 10) / 10;
            maxScale = Math.ceil(minMax.max * 10) / 10;
        }
        else if(minMax.max - minMax.min < 2) {
            minScale = Math.floor(minMax.min * 5) / 5;
            maxScale = Math.ceil(minMax.max * 5) / 5;
        }
        else if(minMax.max - minMax.min < 5) {
            minScale = Math.floor(minMax.min * 2) / 2;
            maxScale = Math.ceil(minMax.max * 2) / 2;
        }
        else {
            minScale = Math.floor(minMax.min);
            maxScale = Math.ceil(minMax.max);
        }

        canvas.maxScale = maxScale;

        canvas.yStep = (canvas.height - 2*canvas.chartMargin) / (maxScale - minScale);

        if(canvas.showGrid === true) {
            drawGrid(ctx, minScale, maxScale);
        }

        if(dataSets.length > 1) {       // only draw graph if there are at least two points
            switch(canvas.lineType) {
                case "bezier":
                    drawBezierGraph(ctx, dataSets, maxScale, canvas.xStep, canvas.yStep);
                    break;
                case "line":
                default:
                    drawLineGraph(ctx, dataSets, maxScale, canvas.xStep, canvas.yStep);
            }
        }

        if(canvas.showDots === true || dataSets.length  < 2) {
            drawDots(ctx, dataSets, maxScale, canvas.xStep, canvas.yStep)
        }

        drawLabels(ctx, minScale, maxScale);
    }

    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        visible: canvas.dataSets.length === 0

        color: canvas.labelColor
        font.family: canvas.labelFontFamily
        font.pixelSize: canvas.labelFontSizeTitle

        text: canvas.chartEmptyInfoText
    }

    Item {
        id: toolTip
        visible: false
        z: 100

        property bool inhibit: true
        property int margin: 10
        property int triangleSize: 12
        property string text: ""
        property var eventPosition: ({x: 0, y: 0})

        readonly property int north: 0
        readonly property int east: 1
        readonly property int south: 2
        readonly property int west: 3

        onEventPositionChanged: {
            if (toolTip.inhibit) return;

            toolTipBackgroundRect.x = 0;
            toolTipBackgroundRect.y = 0;
            var orientation = toolTip.north;

            // determine orientation
            if(toolTip.eventPosition.x < canvas.chartMargin + toolTip.triangleSize) orientation = toolTip.east;
            else if(toolTip.eventPosition.x > canvas.width - (canvas.chartMargin + toolTip.triangleSize)) orientation = toolTip.west;
            else if(toolTip.eventPosition.y - toolTipBackgroundRect.height < canvas.chartMargin + toolTip.triangleSize) orientation = toolTip.south;
            else orientation = toolTip.north;

            toolTipBackgroundRect.anchors.top = undefined;
            toolTipBackgroundRect.anchors.bottom = undefined;
            toolTipBackgroundRect.anchors.left = undefined;
            toolTipBackgroundRect.anchors.right = undefined;

            toolTipTriangle.anchors.verticalCenter = undefined;
            toolTipTriangle.anchors.horizontalCenter = undefined;

            var xPosition = 0;
            var yPosition = 0;

            switch(orientation) {
            case toolTip.north:
                xPosition = toolTip.eventPosition.x - toolTipBackgroundRect.width/2;
                if(xPosition < canvas.chartMargin + toolTip.triangleSize) xPosition = canvas.chartMargin;
                if(xPosition + toolTipBackgroundRect.width > canvas.width - (canvas.chartMargin + toolTip.triangleSize)) xPosition = canvas.width - toolTipBackgroundRect.width - canvas.chartMargin;
                yPosition = toolTip.eventPosition.y - toolTipBackgroundRect.height - toolTip.triangleSize;

                toolTip.x = Math.round(xPosition);
                toolTip.y = Math.round(yPosition);

                toolTip.width = toolTipBackgroundRect.width;
                toolTip.height = toolTipBackgroundRect.height + triangleSize;

                toolTipBackgroundRect.anchors.top = toolTipFullRect.top;
                toolTipBackgroundRect.anchors.left = toolTipFullRect.left;

                toolTipTriangle.anchors.verticalCenter = toolTipContentRect.bottom;
                break;
            case toolTip.south:
                xPosition = toolTip.eventPosition.x - toolTipBackgroundRect.width/2;
                if(xPosition < canvas.chartMargin + toolTip.triangleSize) xPosition = canvas.chartMargin;
                if(xPosition + toolTipBackgroundRect.width > canvas.width - (canvas.chartMargin + toolTip.triangleSize)) xPosition = canvas.width - toolTipBackgroundRect.width - canvas.chartMargin;
                yPosition= toolTip.eventPosition.y;

                toolTip.x = Math.round(xPosition);
                toolTip.y = Math.round(yPosition);

                toolTip.width = toolTipBackgroundRect.width;
                toolTip.height = toolTipBackgroundRect.height + triangleSize;

                toolTipBackgroundRect.anchors.bottom = toolTipFullRect.bottom;
                toolTipBackgroundRect.anchors.left = toolTipFullRect.left;

                toolTipTriangle.anchors.verticalCenter = toolTipContentRect.top;
                break;
            case toolTip.east:
                xPosition = toolTip.eventPosition.x;
                yPosition = toolTip.eventPosition.y - toolTipBackgroundRect.height/2;

                toolTip.x = Math.round(xPosition);
                toolTip.y = Math.round(yPosition);

                toolTip.width = toolTipBackgroundRect.width + triangleSize;
                toolTip.height = toolTipBackgroundRect.height;

                toolTipBackgroundRect.anchors.top = toolTipFullRect.top;
                toolTipBackgroundRect.anchors.right = toolTipFullRect.right;

                toolTipTriangle.anchors.horizontalCenter = toolTipContentRect.left;
                break;
            case toolTip.west:
                xPosition = toolTip.eventPosition.x - toolTipBackgroundRect.width - triangleSize;
                yPosition = toolTip.eventPosition.y - toolTipBackgroundRect.height/2;

                toolTip.x = Math.round(xPosition);
                toolTip.y = Math.round(yPosition);

                toolTip.width = toolTipBackgroundRect.width + triangleSize;
                toolTip.height = toolTipBackgroundRect.height;

                toolTipBackgroundRect.anchors.top = toolTipFullRect.top;
                toolTipBackgroundRect.anchors.left = toolTipFullRect.left;

                toolTipTriangle.anchors.horizontalCenter = toolTipContentRect.right;
                break;
            }

            toolTip.visible = true;
        }

        Component.onCompleted: toolTip.inhibit = false

        Rectangle {
            id: toolTipFullRect
            anchors.fill: parent
            layer.enabled: true     // render all elements to a buffer and then apply the opacity
            opacity: 0.92
            color: "transparent"

            Rectangle {
                id: toolTipBackgroundRect
                width: toolTipContentRect.width + 4
                height: toolTipContentRect.height + 4
                radius: 4
                color: canvas.gridColor

                Rectangle {
                    id: toolTipContentRect
                    width: toolTipSizeHelper.width + toolTip.margin
                    height: toolTipSizeHelper.height + toolTip.margin
                    anchors.centerIn: parent
                    radius: 3
                    z: 1
                    color: canvas.labelColor

                    Text {
                        id: toolTipText
                        anchors.centerIn: parent
                        color: canvas.dotColor
                        text: toolTip.text
                    }

                    Text {
                        id: toolTipSizeHelper
                        visible: false
                        text: toolTip.text
                    }

                    MouseArea {
                        id: toolTipMouseArea
                        anchors.fill: parent

                        onPressed: {
                            mouse.accepted = true;
                            hideToolTip(true);
                        }
                    }
                }

                Rectangle {
                    id: toolTipTriangle
                    width: toolTip.triangleSize
                    height: width
                    rotation: 45
                    x: toolTip.eventPosition.x - toolTip.x - width/2
                    y: toolTip.eventPosition.y - toolTip.y - height/2
                    z: 0
                    color: canvas.labelColor
                    border.color: canvas.gridColor
                    border.width: 2
                }
            }
        }
    }

    MouseArea {
        id: canvasMouseArea
        anchors.fill: parent

        property int xPressed: 0
        property int yPressed: 0

        onPressed: {
            mouse.accepted = true;
            xPressed = mouse.x;
            yPressed = mouse.y;
        }

        onReleased: {
            mouse.accepted = true;
            if(Math.abs(mouse.x - xPressed) < 15 && Math.abs(mouse.y - yPressed) < 15) {
                // find nearest data point
                var neareastXValue = canvas.dataSets[0][canvas.dataIds.x] + Math.round((mouse.x - canvas.chartMargin) / canvas.xStep);
                var minX = neareastXValue - canvas.dataSets[0][canvas.dataIds.x];
                var tempMinX = 0;
                var dataIndex = 0;
                var tempDataIndex = 1;
                for (; tempDataIndex < canvas.dataSets.length; tempDataIndex++) {
                    tempMinX = Math.abs(neareastXValue - canvas.dataSets[tempDataIndex][canvas.dataIds.x]);
                    if(tempMinX < minX) {
                        minX = tempMinX;
                        dataIndex = tempDataIndex;
                    }
                    else break;
                }

                var deltaX = 0;
                var deltaY = 0;
                var delta = 0;
                var deltaMin = 100000;
                var deltaMinIndex = 0;
                var i = dataIndex - 10 < 0 ? 0 : dataIndex - 10;
                var end = dataIndex + 10 > canvas.dataSets.length - 1 ? canvas.dataSets.length : dataIndex + 10;
                for(; i < end; i++) {
                    deltaX = Math.abs(mouse.x - Math.round((canvas.dataSets[i][canvas.dataIds.x] - canvas.dataSets[0][canvas.dataIds.x]) * canvas.xStep + chartMargin));
                    deltaY = Math.abs(mouse.y - Math.round(chartMargin + (canvas.maxScale - canvas.dataSets[i][canvas.dataIds.y]) * yStep));
                    delta = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
                    if(delta < deltaMin) {
                        deltaMin = delta;
                        deltaMinIndex = i;
                    }
                }

                showToolTip(deltaMinIndex, mouse.x, mouse.y, true)
            }
            else {
                hideToolTip(true);
            }
        }
    }

    function showToolTip(dataIndex, eventPositionX, eventPositionY, triggerEvent) {
        triggerEvent = (typeof triggerEvent !== 'undefined') ? triggerEvent : false;
        var dataValue = canvas.dataSets[dataIndex][canvas.dataIds.y];
        var dataString = dataValue.toFixed(2).replace(".", Qt.locale().decimalPoint);
        var annotationString = canvas.dataSets[dataIndex]["annotation"];
        if(annotationString.length > 0) annotationString = "\n" + annotationString;
        var dataPointX = Math.round((canvas.dataSets[dataIndex][canvas.dataIds.x] - canvas.dataSets[0][canvas.dataIds.x]) * canvas.xStep + chartMargin);
        var dataPointY = Math.round(chartMargin + (canvas.maxScale - dataValue) * yStep);
        if((Math.abs(eventPositionX - dataPointX) < 30 && Math.abs(eventPositionY - dataPointY) < 30) || !triggerEvent) { // only show if the data point is near the event position or if the event was not triggered by this component
            toolTip.text = Database.getDateFromDayAndTimeId(canvas.dataSets[dataIndex][canvas.dataIds.x], canvas.dataSets[dataIndex]["timeId"]).toLocaleString(Qt.locale(), Locale.ShortFormat) + " → " + dataString + canvas.units.y + annotationString;
            toolTip.eventPosition = {x: dataPointX, y: dataPointY};
            if(triggerEvent) {
                signalShowToolTip(dataIndex, eventPositionX, eventPositionY, false);
            }
        }
        else {
            hideToolTip(triggerEvent);
        }
    }

    function hideToolTip (triggerEvent) {
        triggerEvent = (typeof triggerEvent !== 'undefined') ? triggerEvent : false;
        if(triggerEvent) signalHideToolTip(false);
        toolTip.visible = false;
    }
}
