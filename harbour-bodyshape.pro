# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-bodyshape

CONFIG += sailfishapp

SOURCES += src/harbour-bodyshape.cpp

OTHER_FILES += qml/harbour-bodyshape.qml \
    qml/cover/CoverPage.qml \
    qml/pages/Overview.qml \
    qml/pages/AddDataSet.qml \
    qml/pages/ShowAsList.qml \
    qml/pages/Settings.qml \
    qml/pages/Info.qml \
    qml/pages/License.qml \
    qml/pages/Chart.qml \
    qml/pages/Database.js \
    rpm/harbour-bodyshape.changes \
    rpm/harbour-bodyshape.spec \
    rpm/harbour-bodyshape.yaml \
    translations/*.ts \
    harbour-bodyshape.desktop

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-bodyshape-de.ts

