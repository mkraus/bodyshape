/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

import "Database.js" as Database

Page {
    id: page

    property var dataSets: Database.getAllDataSets()
    property var units: Database.getUnits()

    property string itemColor: Theme.secondaryHighlightColor
    property string itemFontFamily: Theme.fontFamily
    property int headerFontSize: Theme.fontSizeExtraSmall
    property int itemFontSize: Theme.fontSizeExtraSmall

    Component.onCompleted: dataList.positionViewAtEnd()

    SilicaFlickable {
        anchors.fill: parent

        Column {
            id: column
            anchors.fill: parent
            spacing: Theme.paddingSmall

            PageHeader {
                id: header
                title: qsTr("Listing")
            }

            ViewPlaceholder {
                enabled: page.dataSets.length === 0
                text: qsTr("No datasets available")
            }

            //Component {        //TODO: Qt5.4
            Row {
                id: dataListHeader
                width: parent.width
                height: dateTimeHeader.height
                //Row {        //TODO: Qt5.4
                Label {
                    id: dateTimeHeader
                    width: dataList.dateTimeWidth
                    color: itemColor
                    font.family: itemFontFamily
                    font.pixelSize: headerFontSize
                    horizontalAlignment: Text.AlignLeft
                    elide: Text.ElideRight

                    text: qsTr("Date")
                }
                Label {
                    width: dataList.weightWidth
                    color: itemColor
                    font.family: itemFontFamily
                    font.pixelSize: headerFontSize
                    horizontalAlignment: Text.AlignRight
                    elide: Text.ElideRight

                    text: qsTr("Weight") + " [" + units.weight + "]"
                }
                Label {
                    width: dataList.fatWidth
                    color: itemColor
                    font.family: itemFontFamily
                    font.pixelSize: headerFontSize
                    horizontalAlignment: Text.AlignRight
                    elide: Text.ElideRight

                    text: qsTr("Fat") + " [" + units.fat + "]"
                }
                Label {
                    width: dataList.muscleWidth
                    color: itemColor
                    font.family: itemFontFamily
                    font.pixelSize: headerFontSize
                    horizontalAlignment: Text.AlignRight
                    elide: Text.ElideRight

                    text: qsTr("Muscle") + " [" + units.muscle + "]"
                }
                //}        //TODO: Qt5.4
            }

            SilicaListView {
                id: dataList
                width: parent.width
                height: page.height - header.height - dataListHeader.height - 2*column.spacing
                //verticalLayoutDirection: ListView.BottomToTop
                highlightFollowsCurrentItem: true
                //headerPositioning: ListView.OverlayHeader     //TODO: only available with Qt5.4; use this when sailfishos is upgraded to this Qt version
                model: page.dataSets
                clip: true
                quickScroll: true

                property int dateTimeWidth: 0.25 * width
                property int weightWidth: 0.25 * width
                property int fatWidth: 0.25 * width
                property int muscleWidth: 0.25 * width

                VerticalScrollDecorator{}

                //header: dataListHeader        //TODO: Qt5.4

                delegate: Row {
                    width: parent.width
                    height: dateTimeLabel.height
                    property date dateTime: Database.getDateFromDayAndTimeId(modelData.dayId, modelData.timeId)
                    Label {
                        id: dateTimeLabel
                        width: dataList.dateTimeWidth
                        color: itemColor
                        font.family: itemFontFamily
                        font.pixelSize: itemFontSize
                        horizontalAlignment: Text.AlignLeft
                        //elide: Text.ElideRight

                        text: dateTime.toLocaleString(Qt.locale(), Locale.ShortFormat)
                    }
                    Label {
                        id: weightLabel
                        width: dataList.weightWidth
                        color: itemColor
                        font.family: itemFontFamily
                        font.pixelSize: itemFontSize
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight

                        text: modelData.weight.toFixed(2).replace(".", Qt.locale().decimalPoint);
                    }
                    Label {
                        id: fatLabel
                        width: dataList.fatWidth
                        color: itemColor
                        font.family: itemFontFamily
                        font.pixelSize: itemFontSize
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight

                        text: modelData.fat.toFixed(2).replace(".", Qt.locale().decimalPoint);
                    }
                    Label {
                        id: muscleLabel
                        width: dataList.muscleWidth
                        color: itemColor
                        font.family: itemFontFamily
                        font.pixelSize: itemFontSize
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight

                        text: modelData.muscle.toFixed(2).replace(".", Qt.locale().decimalPoint);
                    }
                }
                onCountChanged: {positionViewAtEnd()}
            }

        }
    }
}

