<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_US">
<context>
    <name>AddDataSet</name>
    <message>
        <source>Update</source>
        <translation>Aktuallisieren</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <source>There is already a data set for this day!</source>
        <translation>Für diesen Tag ist bereits ein Datensatz vorhanden!</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <source>Body fat</source>
        <translation>Körperfett</translation>
    </message>
    <message>
        <source>Muscle mass</source>
        <translation>Muskelmasse</translation>
    </message>
    <message>
        <source>Annotation</source>
        <translation>Anmerkung</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Body Shape</source>
        <translation>Body Shape</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>The application allows you to keep track of your weight, body fat and muscle mass.</source>
        <translation>Diese Anwendung erlaubt es, einen Überblick über das Gewicht, Körperfett und Muskelmasse zu behalten.</translation>
    </message>
    <message>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the &lt;b&gt;GNU General Public License&lt;/b&gt; as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</source>
        <translation>Diese Anwendung is freie Software; Sie dürfen sie weiter verbreiten und/oder modifizieren unter den Bedingungen der &lt;b&gt;GNU General Public Lincense&lt;/b&gt; wie sie von der Free Software Foundation veröffentlicht wurde; entweder version 2 der Lizenz oder (von ihnen wählbar) eine neuere Version.</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Quelltext</translation>
    </message>
</context>
<context>
    <name>License</name>
    <message>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <source>Add data set</source>
        <translation>Datensatz hinzufügen</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Show as list</source>
        <translation>Zeige als Liste</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <source>Body Fat</source>
        <translation>Körperfett</translation>
    </message>
    <message>
        <source>Muscle Mass</source>
        <translation>Muskelmasse</translation>
    </message>
    <message>
        <source>No datasets available</source>
        <translation>Keine Datensätze vorhanden</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Display body fat in</source>
        <translation>Körperfett in</translation>
    </message>
    <message>
        <source>Display muscle mass in</source>
        <translation>Muskelmasse in</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Moving average</source>
        <translation>Gleitender Mittelwert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Aus</translation>
    </message>
    <message>
        <source>10 Points</source>
        <translation>10 Punkte</translation>
    </message>
    <message>
        <source>Delete all data sets</source>
        <translation>Alle Datensätze löschen</translation>
    </message>
    <message>
        <source>Body Fat</source>
        <translation>Körperfett</translation>
    </message>
    <message>
        <source>Gather body fat</source>
        <translation>Körperfett erfassen</translation>
    </message>
    <message>
        <source>Show body fat chart</source>
        <translation>Körperfettdiagramm anzeigen</translation>
    </message>
    <message>
        <source>Muscle Mass</source>
        <translation>Muskelmasse</translation>
    </message>
    <message>
        <source>Gather muscle mass</source>
        <translation>Muskelmasse erfassen</translation>
    </message>
    <message>
        <source>Show muscle mass chart</source>
        <translation>Muskelmassendiagramm anzeigen</translation>
    </message>
</context>
<context>
    <name>ShowAsList</name>
    <message>
        <source>Listing</source>
        <translation>Auflistung</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <source>Fat</source>
        <translation>Fett</translation>
    </message>
    <message>
        <source>Muscle</source>
        <translation>Muskeln</translation>
    </message>
    <message>
        <source>No datasets available</source>
        <translation>Keine Datensätze vorhanden</translation>
    </message>
</context>
</TS>
