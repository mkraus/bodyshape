/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

import "Database.js" as Database

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                id: deleteAll
                text: qsTr("Delete all data sets")
                onClicked: {
                    remorse.execute(header, deleteAll.text, function() {
                        Database.deleteAllDataSets()
                    } )

                    Database.updateCallback();
                }
            }
        }

        RemorseItem { id: remorse }

        contentHeight: column.height

        VerticalScrollDecorator{}

        Column {
            id: column
            width: page.width

            anchors.leftMargin: Theme.paddingLarge
            anchors.rightMargin: Theme.paddingLarge

            PageHeader {
                id: header
                title: qsTr("Settings")
            }

            ComboBox {
                id: smaPoints
                width: column.width
                label: qsTr("Moving average")
                currentIndex: Database.getSetting("SmaPoints", "1") === "1" ? 0 : 1

                menu: ContextMenu {
                    MenuItem { text: qsTr("Disabled") }
                    MenuItem { text: qsTr("10 Points") }
                }

                onCurrentIndexChanged: {
                    Database.setSetting("SmaPoints", (smaPoints.currentIndex === 0) ? "1" : "10");
                    Database.updateCallback();
                }
            }

            Label {
                color: Theme.highlightColor
                anchors {
                    right: parent.right
                    rightMargin: Theme.paddingLarge
                }
                font {
                    family: Theme.fontFamilyHeading
                    pixelSize: Theme.fontSizeLarge
                }
                text: qsTr("Body Fat")
            }

            TextSwitch {
                id: gatherFatData
                width: column.width
                text: qsTr("Gather body fat")

                checked: Database.getSetting("gatherFatData", "1") === "1" ? true : false

                onCheckedChanged: {
                    Database.setSetting("gatherFatData", gatherFatData.checked ? "1" : "0");
                    Database.updateCallback();
                }
            }

            TextSwitch {
                id: showFatChart
                width: column.width
                text: qsTr("Show body fat chart")

                checked: Database.getSetting("showFatChart", "1") === "1" ? true : false

                onCheckedChanged: {
                    Database.setSetting("showFatChart", showFatChart.checked ? "1" : "0");
                    Database.updateCallback();
                }
            }

            ComboBox {
                id: bodyFatPresentation
                width: column.width
                label: qsTr("Display body fat in")
                currentIndex: Database.getSetting("fatPresentation", "absolute") === "absolute" ? 0 : 1

                menu: ContextMenu {
                    MenuItem { text: "kg" }
                    MenuItem { text: "%" }
                }

                onCurrentIndexChanged: {
                    Database.setSetting("fatPresentation", (bodyFatPresentation.currentIndex === 0) ? "absolute" : "relative");
                    Database.updateCallback();
                }
            }

            Label {
                color: Theme.highlightColor
                anchors {
                    right: parent.right
                    rightMargin: Theme.paddingLarge
                }
                font {
                    family: Theme.fontFamilyHeading
                    pixelSize: Theme.fontSizeLarge
                }
                text: qsTr("Muscle Mass")
            }

            TextSwitch {
                id: gatherMuscleData
                width: column.width
                text: qsTr("Gather muscle mass")

                checked: Database.getSetting("gatherMuscleData", "1") === "1" ? true : false

                onCheckedChanged: {
                    Database.setSetting("gatherMuscleData", gatherMuscleData.checked ? "1" : "0");
                    Database.updateCallback();
                }
            }

            TextSwitch {
                id: showMuscleChart
                width: column.width
                text: qsTr("Show muscle mass chart")

                checked: Database.getSetting("showMuscleChart", "1") === "1" ? true : false

                onCheckedChanged: {
                    Database.setSetting("showMuscleChart", showMuscleChart.checked ? "1" : "0");
                    Database.updateCallback();
                }
            }

            ComboBox {
                id: muscleMassPresentation
                width: column.width
                label: qsTr("Display muscle mass in")
                currentIndex: Database.getSetting("musclePresentation", "absolute") === "absolute" ? 0 : 1

                menu: ContextMenu {
                    MenuItem { text: "kg" }
                    MenuItem { text: "%" }
                }

                onCurrentIndexChanged: {
                    Database.setSetting("musclePresentation", (muscleMassPresentation.currentIndex === 0) ? "absolute" : "relative");
                    Database.updateCallback();
                }
            }

        }
    }
}





