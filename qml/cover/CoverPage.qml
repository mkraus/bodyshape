/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../pages"
import "../pages/Database.js" as Database

CoverBackground {

    Item {
       width: parent.width
       height: parent.height / 3
       anchors.top: parent.top

        Label {
            id: label
            anchors.centerIn: parent
            text: qsTr("Body Shape")
        }
    }


    Item {
        width: parent.width
        height: parent.height * 2 / 3
        anchors.bottom: parent.bottom

        Chart {
            id: weightChart
            width: parent.width
            height: parent.height * 2 / 3
            anchors.top: parent.top

            chartMargin: Theme.paddingLarge

            chartTitle: ""
            dataSets: Database.dataSetCache
            showLabelX: false
            showLabelY: false
            showGrid: false
            dataIds: ({x: "dayId", y: "weight"})
            lineType: "bezier"
            showDots: false
            lineColor: Theme.highlightColor
        }
    }
}


