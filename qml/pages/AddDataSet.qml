/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

import "Database.js" as Database

Dialog {
    id: page

    onAccepted: {
        var now = new Date();
        Database.addDataSet(Database.getDayIdFromDate(now), Database.getTimeIdFromDate(now), weight.text.replace(Qt.locale().decimalPoint,"."), fat.text.replace(Qt.locale().decimalPoint,"."), muscle.text.replace(Qt.locale().decimalPoint,"."), annotation.text);
    }

    Component.onCompleted: {
        var dayId = Database.getDayIdFromDate(new Date());
        if(Database.isDayIdAssigned(dayId) > 0) {
            var dataset = Database.getDataSet(dayId);
            weight.text = dataset.weight;
            weight.text = weight.text.replace(".", Qt.locale().decimalPoint);
            fat.text = dataset.fat;
            fat.text = fat.text.replace(".", Qt.locale().decimalPoint);
            muscle.text = dataset.muscle;
            muscle.text = muscle.text.replace(".", Qt.locale().decimalPoint);
            annotation.text = dataset.annotation;
            dialogHeader.acceptText = qsTr("Update");
            datasetAvailable.visible = true;
        }
        else {
            dialogHeader.acceptText = qsTr("Add");
            datasetAvailable.visible = false;
        }
    }

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: column.height

        Column {
            id: column
            width: page.width

            spacing: Theme.paddingLarge

            DialogHeader {
                id: dialogHeader
                acceptText: qsTr("Add")
            }

            VerticalScrollDecorator{}

            Label {
                id: datasetAvailable
                visible: false
                text: qsTr("There is already a data set for this day!")
                wrapMode: Text.WordWrap
                color: Theme.highlightColor
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }
            }

            TextField {
                id: weight
                width: column.width
                focus: true
                inputMethodHints: Qt.ImhDigitsOnly
                placeholderText: qsTr("Weight") + " [kg]"
                label: placeholderText
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: fat.focus = true
            }

            TextField {
                id: fat
                width: column.width
                visible: Database.getSetting("gatherFatData", "1") === "1" ? true : false
                inputMethodHints: Qt.ImhDigitsOnly
                placeholderText: qsTr("Body fat") + " [%]"
                label: placeholderText
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: muscle.focus = true
            }

            TextField {
                id: muscle
                width: column.width
                visible: Database.getSetting("gatherMuscleData", "1") === "1" ? true : false
                inputMethodHints: Qt.ImhDigitsOnly
                placeholderText: qsTr("Muscle mass") + " [%]"
                label: placeholderText
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: annotation.focus = true
            }

            TextArea {
                id: annotation
                width: column.width
                placeholderText: qsTr("Annotation")
                label: placeholderText
            }
        }
    }
}





