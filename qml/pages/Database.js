/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

.pragma library
.import QtQuick.LocalStorage 2.0 as LS

// list all tables:     SELECT name FROM SQLITE_MASTER
// delete table:        DROP TABLE WeightDataSets
// create table:        CREATE TABLE IF NOT EXISTS WeightDataSets(dayId INTEGER, timeId INTEGER, weight NUMERIC(5,2), fat NUMERIC(5,2), muscle NUMERIC(5,2))
// add data:            INSERT INTO WeightDataSets VALUES(16384, 0818, 76.4, 12.2, 45.8)
// query data:          SELECT * FROM WeightDataSets ORDER BY dayId, timeId
// query data range:    SELECT weight FROM WeightDataSets WHERE dayId > 16384
// query average:       SELECT AVG(weight) FROM WeightDataSets WHERE dayId BETWEEN 16384 AND 16484
// delete entry:        DELETE FROM WeightDataSets WHERE dayId = 16384 AND timeId BETWEEN 480 AND 540
// update entry:        UPDATE WeightDataSets SET weight = 72.8 WHERE dayId = 16384 AND timeId = 908
// add setting:         INSERT INTO Settings VALUES ('musclePresentation', 'absolute')
// change setting:      UPDATE Settings SET value = 'absolute' WHERE key = 'musclePresentation'

var dataSetCache = [];

var updateCallback = null;

function setUpdateCallback(callback) {
    updateCallback = callback;
}

var databaseInitialized = 0;

function openDatabase() {
    var db = LS.LocalStorage.openDatabaseSync("BodyShape", "", "Storage for the weight, body fat and muscle mass", 1000000);
    if(databaseInitialized === 0) {
        // for database updates, check db.version and update with db.changeVersion

        if (db.version === "") {
            db.changeVersion("", "1.0", function(tx) {
                // create settings table
                tx.executeSql('CREATE TABLE IF NOT EXISTS Settings(key TEXT, value TEXT)');
                // create data sets table
                tx.executeSql('CREATE TABLE IF NOT EXISTS WeightDataSets(dayId INTEGER, timeId INTEGER, weight NUMERIC(5,2), fat NUMERIC(5,2), muscle NUMERIC(5,2))');
            });
        }

        if (db.version === "1.0") {
            db.changeVersion("1.0", "1.1", function(tx) {
                // add annotation table --- attention, TEXT is only 256 characters long
                tx.executeSql('ALTER TABLE WeightDataSets ADD annotation TEXT NOT NULL DEFAULT ""');
            });
        }

        databaseInitialized = 1;
    }
    
    return db;
}

function setSetting(key, value) {
    var db = openDatabase();
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT * from Settings where key = ?', [key]);
        if (rs.rows.length > 0) {
            tx.executeSql('UPDATE Settings SET value = ? where key = ?', [value, key]);
        }
        else {
            tx.executeSql('INSERT INTO Settings VALUES(?, ?)', [key, value]);
        }
    });
}

function getSetting(key, defaultValue) {
   var db = openDatabase();
   var returnValue = defaultValue;
   db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT value FROM Settings WHERE key=?', [key]);
        if (rs.rows.length > 0) {
            returnValue = rs.rows.item(0).value;
        }
  });
   
  return returnValue;
}

function getUnits() {
    var weightUnit = "kg";
    var fatUnit = getSetting("fatPresentation", "relative") === "relative" ? "%" : "kg";
    var muscleUnit = getSetting("musclePresentation", "relative") === "relative" ? "%" : "kg";
    return {weight: weightUnit, fat: fatUnit, muscle: muscleUnit};
}

function deleteAllDataSets() {
    var db = openDatabase();
    db.transaction(function(tx) {
        tx.executeSql('DELETE FROM WeightDataSets');
    });

    if(updateCallback != null) updateCallback();
}

function getDayIdFromDate(date) {
    var dateAtMidnight = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    var dayId = (dateAtMidnight.getTime() / (60*1000) - dateAtMidnight.getTimezoneOffset()) / (24*60);     // convert from JS date
    return Math.round(dayId)
}
function getDateFromDayId(dayId) {
    var date = new Date(dayId * (24*60*60*1000));   // convert to JS date
    return new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);     // remove timezone offset to get 0 o'clock
}
function getDateFromDayAndTimeId(dayId, timeId) {
    var date = getDateFromDayId(dayId);
    date.setMinutes(timeId);
    return date;
}
function getTimeIdFromDate(date) {
    var timeId = date.getHours() * 60 + date.getMinutes();
    return timeId;
}

function isDayIdAssigned(dayId) {
    var db = openDatabase();
    var returnValue = 0;
    db.transaction(function(tx) {
        returnValue = tx.executeSql('SELECT * FROM WeightDataSets WHERE dayId = ?', [dayId]).rows.length > 0;
    });
    return returnValue;
}

function getDataSet(dayId) {
    var db = openDatabase();
    var dataSet = { weight: 0, fat: 0, muscle:0, annotation:"" }
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT * FROM WeightDataSets WHERE dayId = ?', [dayId]);
        if(rs.rows.length > 0) {
            dataSet.weight = rs.rows.item(0).weight;
            dataSet.fat = rs.rows.item(0).fat;
            dataSet.muscle = rs.rows.item(0).muscle;
            dataSet.annotation = rs.rows.item(0).annotation;
        }
    });
    return dataSet;
}

function getDataSets(minWeight, maxWeight) {
    var dataSetSmaValues = [];
    dataSetCache = [];
    var fatPresentation = getSetting("fatPresentation", "relative");
    var musclePresentation = getSetting("musclePresentation", "relative");
    var smaPoints = Number(getSetting("SmaPoints", "1"));

    var db = openDatabase();
    db.transaction(function(tx) {
        // Show all data sets
        var rs;
        var min;
        var max;
        /*rs = tx.executeSql('SELECT MAX(dayId), weight FROM WeightDataSets');
        min = rs.rows.item(0).weight - 0.5;
        max = rs.rows.item(0).weight + 0.5;
        rs = tx.executeSql('SELECT * FROM WeightDataSets WHERE weight BETWEEN ? AND ?', [min, max]);*/
        rs = tx.executeSql('SELECT * FROM WeightDataSets WHERE weight BETWEEN ? AND ? ORDER BY dayId, timeId', [minWeight, maxWeight]);

        if (rs.rows.length > 0) {
            var log = ""
            var date = 0
            var year = 0
            var month = 0
            var day = 0
            var dbDataSet;
            var weight = 0;
            var fat = 0;
            var muscle = 0;


            for(var i = 0; i < rs.rows.length; i++) {
                dbDataSet = rs.rows.item(i);
                
                weight = dbDataSet.weight;
                fat = fatPresentation === "relative" ? dbDataSet.fat : weight * dbDataSet.fat / 100;
                muscle = musclePresentation === "relative" ? dbDataSet.muscle : weight * dbDataSet.muscle / 100;
                dataSetCache[i] = {dayId: dbDataSet.dayId, timeId: dbDataSet.timeId, weight: weight, fat: fat, muscle: muscle, annotation: dbDataSet.annotation}
                dataSetSmaValues[i] = {weight: weight, fat: fat, muscle: muscle}
            }

            var sumWeight = 0;
            var sumFat = 0;
            var sumMuscle = 0;
            for(var i = 0; i < 5 && i < dataSetSmaValues.length; i++) {
                sumWeight += dataSetSmaValues[i]["weight"];
                sumFat += dataSetSmaValues[i]["fat"];
                sumMuscle += dataSetSmaValues[i]["muscle"];
            }

            if(smaPoints > 1) {
                for(var i = 0; i < dataSetSmaValues.length; i++) {
                    if(i < 5) {
                        sumWeight += dataSetSmaValues[i+5]["weight"];
                        dataSetCache[i]["weight"] = sumWeight / (i+6);
                        sumFat += dataSetSmaValues[i+5]["fat"];
                        dataSetCache[i]["fat"] = sumFat / (i+6);
                        sumMuscle += dataSetSmaValues[i+5]["muscle"];
                        dataSetCache[i]["muscle"] = sumMuscle / (i+6);
                    } else if(i < dataSetSmaValues.length - 5) {
                        sumWeight = sumWeight - dataSetSmaValues[i-5]["weight"] + dataSetSmaValues[i+5]["weight"];
                        dataSetCache[i]["weight"] = sumWeight / 10;
                        sumFat = sumFat - dataSetSmaValues[i-5]["fat"] + dataSetSmaValues[i+5]["fat"];
                        dataSetCache[i]["fat"] = sumFat / 10;
                        sumMuscle = sumMuscle - dataSetSmaValues[i-5]["muscle"] + dataSetSmaValues[i+5]["muscle"];
                        dataSetCache[i]["muscle"] = sumMuscle / 10;
                    } else {
                        sumWeight = sumWeight - dataSetSmaValues[i-5]["weight"];
                        dataSetCache[i]["weight"] = sumWeight / (dataSetSmaValues.length - i + 4);
                        sumFat = sumFat - dataSetSmaValues[i-5]["fat"];
                        dataSetCache[i]["fat"] = sumFat / (dataSetSmaValues.length - i + 4);
                        sumMuscle = sumMuscle - dataSetSmaValues[i-5]["muscle"];
                        dataSetCache[i]["muscle"] = sumMuscle / (dataSetSmaValues.length - i + 4);
                    }
                }
            }

        }
    });

    return dataSetCache;
}

function getAllDataSets() {
    return getDataSets(0, 1000);
}

function addDataSet(dayId, timeId, weight, fat, muscle, annotation) {
    var db = openDatabase();
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT * from WeightDataSets where dayId = ?', [dayId]);
        if (rs.rows.length > 0) {
            tx.executeSql('UPDATE WeightDataSets SET timeId = ?, weight = ?, fat = ?, muscle = ?, annotation = ? where dayId = ?', [timeId, weight, fat, muscle, annotation, dayId]);
        }
        else {
            tx.executeSql('INSERT INTO WeightDataSets VALUES(?, ?, ?, ?, ?, ?)', [dayId, timeId, weight, fat, muscle, annotation]);
        }
    });

    if(updateCallback != null) updateCallback();
}
