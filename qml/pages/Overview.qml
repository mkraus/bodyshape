/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0

import "Database.js" as Database

// import org.kde.kquickcontrolsaddons 2.0 as KQuickAddons // for plasma integration

Page {
    id: page

    property var dataSets: []
    property var unitsWeightChart: ({x: "", y: "kg"})
    property var unitsFatChart: ({x: "", y: "kg"})
    property var unitsMuscleChart: ({x: "", y: "kg"})
    property var labelsX: ({min: "0", max: "10"})

    property bool inhibitRepaint: true

    property int chartHeight: 250
    property bool showDots: true
    property string lineType: "bezier"
    property real bezierFactor: 0.25
    property real slopeFactor: 1.25
    property string lineColor: Theme.highlightColor
    property string dotColor: Theme.primaryColor
    property string gridColor: Theme.secondaryColor
    property string labelColor: Theme.secondaryHighlightColor
    property string labelFontFamily: Theme.fontFamily
    property int labelFontSizeTitle: Theme.fontSizeExtraSmall
    property int labelFontSize: Theme.fontSizeTiny

    property int maxHeight: 0    // page height get reduced if keyboard is visible, therefore store the height in a variable

    function repaintCharts() {
        if(page.inhibitRepaint === false) {
            // get data sets
            page.dataSets = Database.getAllDataSets();

            // get units and labels if there are datasets
            if (page.dataSets.length > 0) {
                // get units
                var units = Database.getUnits();
                page.unitsWeightChart = {x: "", y: units.weight};
                page.unitsFatChart = {x: "", y: units.fat};
                page.unitsMuscleChart = {x: "", y: units.muscle};

                // get x labels
                var minDate = Database.getDateFromDayId(dataSets[0].dayId);
                page.labelsX.min = (minDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat));
                var maxDate = Database.getDateFromDayId(dataSets[dataSets.length-1].dayId);
                page.labelsX.max = (maxDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat));
            }

            // set chart visibility
            var showFatChart = Database.getSetting("showFatChart", "1") === "1" ? true : false;
            var showMuscleChart = Database.getSetting("showMuscleChart", "1") === "1" ? true : false;
            weightChart.visible = true;
            fatChart.visible = showFatChart;
            muscleChart.visible = showMuscleChart;

            // decide where to show the x label
            weightChart.showLabelX = !showFatChart && !showMuscleChart;
            fatChart.showLabelX = !showMuscleChart;
            muscleChart.showLabelX = showMuscleChart;

            // calculate chart height
            var numberOfCharts = 1 + (showFatChart ? 1 : 0) + (showMuscleChart ? 1 : 0);
            chartHeight = (page.maxHeight - header.height - (numberOfCharts+1) * column.spacing) / numberOfCharts;

            // update charts
            weightChart.updateChart();
            if(showFatChart) fatChart.updateChart();
            if(showMuscleChart) muscleChart.updateChart();
        }

        placeholder.enabled = (page.dataSets.length === 0);
    }

    Component.onCompleted: {
        Database.setUpdateCallback(repaintCharts);

        page.maxHeight = page.height;       // cache the page height; this is needed because the keyboard reduces the height and we get wrong chart height after adding a new data set

        page.inhibitRepaint = false;
        page.repaintCharts();
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Info")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("Info.qml"))
                }
            }
            MenuItem {
                text: qsTr("Settings")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("Settings.qml"))
                }
            }
            MenuItem {
                text: qsTr("Show as list")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("ShowAsList.qml"))
                }
            }
            MenuItem {
                text: qsTr("Add data set")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("AddDataSet.qml"))
                }
            }
        }

        VerticalScrollDecorator {}

        contentHeight: column.height

        Column {
            id: column

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                id: header
                title: qsTr("Overview")
            }

            ViewPlaceholder {
                id: placeholder
                enabled: false
                text: qsTr("No datasets available")
            }

            Chart {
                id: weightChart
                width: parent.width
                height: chartHeight
                visible: false

                chartMargin: Theme.paddingLarge

                chartTitle: qsTr("Weight")
                dataSets: page.dataSets
                units: page.unitsWeightChart
                labelsX: page.labelsX
                showLabelX: false
                showLabelY: true
                showGrid: true
                dataIds: ({x: "dayId", y: "weight"})
                lineType: page.lineType
                showDots: page.showDots
                bezierFactor: page.bezierFactor
                slopeFactor: page.slopeFactor
                lineColor: page.lineColor
                dotColor: page.dotColor
                gridColor: page.gridColor
                labelColor: page.labelColor
                labelFontFamily: page.labelFontFamily
                labelFontSizeTitle: page.labelFontSizeTitle
                labelFontSize: page.labelFontSize
            }
            Chart {
                id: fatChart
                width: parent.width
                height: chartHeight
                visible: false

                chartMargin: Theme.paddingLarge

                chartTitle: qsTr("Body Fat")
                dataSets: page.dataSets
                units: page.unitsFatChart
                labelsX: page.labelsX
                showLabelX: false
                showLabelY: true
                showGrid: true
                dataIds: ({x: "dayId", y: "fat"})
                lineType: page.lineType
                showDots: page.showDots
                bezierFactor: page.bezierFactor
                slopeFactor: page.slopeFactor
                lineColor: page.lineColor
                dotColor: page.dotColor
                gridColor: page.gridColor
                labelColor: page.labelColor
                labelFontFamily: page.labelFontFamily
                labelFontSizeTitle: page.labelFontSizeTitle
                labelFontSize: page.labelFontSize
            }
            Chart {
                id: muscleChart
                width: parent.width
                height: chartHeight
                visible: false

                chartMargin: Theme.paddingLarge

                chartTitle: qsTr("Muscle Mass")
                dataSets: page.dataSets
                units: page.unitsMuscleChart
                labelsX: page.labelsX
                showLabelX: true
                showLabelY: true
                showGrid: true
                dataIds: ({x: "dayId", y: "muscle"})
                lineType: page.lineType
                showDots: page.showDots
                bezierFactor: page.bezierFactor
                slopeFactor: page.slopeFactor
                lineColor: page.lineColor
                dotColor: page.dotColor
                gridColor: page.gridColor
                labelColor: page.labelColor
                labelFontFamily: page.labelFontFamily
                labelFontSizeTitle: page.labelFontSizeTitle
                labelFontSize: page.labelFontSize
            }

            Component.onCompleted: {
                weightChart.signalShowToolTip.connect(fatChart.showToolTip);
                weightChart.signalHideToolTip.connect(fatChart.hideToolTip);
                weightChart.signalShowToolTip.connect(muscleChart.showToolTip);
                weightChart.signalHideToolTip.connect(muscleChart.hideToolTip);

                fatChart.signalShowToolTip.connect(weightChart.showToolTip);
                fatChart.signalHideToolTip.connect(weightChart.hideToolTip);
                fatChart.signalShowToolTip.connect(muscleChart.showToolTip);
                fatChart.signalHideToolTip.connect(muscleChart.hideToolTip);

                muscleChart.signalShowToolTip.connect(weightChart.showToolTip);
                muscleChart.signalHideToolTip.connect(weightChart.hideToolTip);
                muscleChart.signalShowToolTip.connect(fatChart.showToolTip);
                muscleChart.signalHideToolTip.connect(fatChart.hideToolTip);
            }

        }
    }
}

