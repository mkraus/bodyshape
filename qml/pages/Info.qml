/*
 * Copyright 2015 Mathias Kraus <k.hias@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: column.height

        Column {
            id: column
            width: page.width

            spacing: Theme.paddingLarge

            PageHeader {
                id: header
                title: qsTr("Info")
            }

            VerticalScrollDecorator{}

            Label {
                wrapMode: TextEdit.WordWrap
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraSmall
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }

                text: qsTr("The application allows you to keep track of your weight, body fat and muscle mass.")
            }

            Label {
                color: Theme.highlightColor
                anchors {
                    right: parent.right
                    rightMargin: Theme.paddingLarge
                }
                font {
                    family: Theme.fontFamilyHeading
                    pixelSize: Theme.fontSizeLarge
                }
                text: qsTr("License")
            }

            BackgroundItem {
                id: licenseItem
                anchors.left: parent.left
                anchors.right: parent.right
                height: licenseLabel.height
                onClicked: pageStack.push(Qt.resolvedUrl("License.qml"))

                Label {
                    id: licenseLabel
                    wrapMode: TextEdit.WordWrap
                    color: licenseItem.highlighted ? Theme.highlightColor : Theme.primaryColor
                    font.pixelSize: Theme.fontSizeExtraSmall
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: Theme.paddingLarge
                    }

                    text: qsTr("This program is free software; you can redistribute it and/or modify it under the terms of the <b>GNU General Public License</b> as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.")
                }
            }

            Label {
                color: Theme.highlightColor
                anchors {
                    right: parent.right
                    rightMargin: Theme.paddingLarge
                }
                font {
                    family: Theme.fontFamilyHeading
                    pixelSize: Theme.fontSizeLarge
                }
                text: qsTr("Source code")
            }

            Label {
                wrapMode: TextEdit.WordWrap
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraSmall
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }

                text: "https://quickgit.kde.org/?p=scratch/mkraus/bodyshape.git"
            }

        }
    }
}





