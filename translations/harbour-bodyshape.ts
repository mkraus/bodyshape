<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AddDataSet</name>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is already a data set for this day!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Body fat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Muscle mass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Annotation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Body Shape</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The application allows you to keep track of your weight, body fat and muscle mass.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the &lt;b&gt;GNU General Public License&lt;/b&gt; as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>License</name>
    <message>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <source>Add data set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show as list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Body Fat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Muscle Mass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No datasets available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Display body fat in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display muscle mass in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Moving average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>10 Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete all data sets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Body Fat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gather body fat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show body fat chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Muscle Mass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gather muscle mass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show muscle mass chart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowAsList</name>
    <message>
        <source>Listing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Muscle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No datasets available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
